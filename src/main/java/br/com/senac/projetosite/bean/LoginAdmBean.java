
package br.com.senac.projetosite.bean;


import br.com.senac.projetosite.dao.AdministradorDAO;
import br.com.senac.projetosite.model.Administrador;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;

@Named(value = "loginAdmBean")
@SessionScoped
public class LoginAdmBean extends Bean{

    private Administrador administrador = new Administrador();
    private Administrador administradorLogado ; 
    private AdministradorDAO dao ;

    public LoginAdmBean() {
    }

    public String toPainel(){
        
        dao = new AdministradorDAO(); 
        
        administradorLogado = dao.findByUsername(administrador.getLoginAdm()) ; 
        
        if(this.administrador.getSenha().equals(administradorLogado.getSenha()) ){
            
            return "painel_administrativo";
        }
        
        
        
        return null;
    }

    public Administrador getAdministrador() {
        return administrador;
    }

    public void setAdministrador(Administrador administrador) {
        this.administrador = administrador;
    }

    public Administrador getAdministradorLogado() {
        return administradorLogado;
    }

    public void setAdministradorLogado(Administrador administradorLogado) {
        this.administradorLogado = administradorLogado;
    }

    public AdministradorDAO getDao() {
        return dao;
    }

    public void setDao(AdministradorDAO dao) {
        this.dao = dao;
    }

    
    
    
    
    

}
