package br.com.senac.projetosite.bean;

import br.com.senac.projetosite.dao.AdministradorDAO;
import br.com.senac.projetosite.model.Administrador;

import java.util.List;
import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.faces.view.ViewScoped;

@Named(value = "cadastroAdministradorBean")
@ViewScoped
public class CadastroAdministradorBean extends Bean {

    private Administrador administrador;
    private AdministradorDAO dao;
    private List<Administrador> lista;

    private Administrador administradorSelecionado;

    private String codigo;
    private String nome;

    private void carregarLista() {
        this.lista = this.dao.findAll();
    }

    public CadastroAdministradorBean() {
        administrador = new Administrador();
        dao = new AdministradorDAO();
        this.carregarLista();
    }

    @PostConstruct
    public void init() {
        try {
            dao = new AdministradorDAO();
            administradorSelecionado = new Administrador();
            lista = dao.findAll();

        } catch (Exception ex) {
            ex.printStackTrace();
            this.addMessageErro("Falha ao carregar itens.");
        }
    }

    public void salvar() {

        if (this.administrador.getId() == 0) {
            dao.save(administrador);
            addMessageInfo("Salvo com sucesso!");
        } else {
            dao.update(administrador);
            addMessageInfo("Alterado com sucesso!");
        }

        this.carregarLista();

    }

    public void novo() {
        this.administrador = new Administrador();
    }

    public Administrador getAdministrador() {
        return administrador;
    }

    public void setAdministrador(Administrador administrador) {
        this.administrador = administrador;
    }

    public AdministradorDAO getDao() {
        return dao;
    }

    public void setDao(AdministradorDAO dao) {
        this.dao = dao;
    }

    public List<Administrador> getLista() {
        return lista;
    }

    public void setLista(List<Administrador> lista) {
        this.lista = lista;
    }

    public Administrador getAdministradorSelecionado() {
        return administradorSelecionado;
    }

    public void setAdministradorSelecionado(Administrador administradorSelecionado) {
        this.administradorSelecionado = administradorSelecionado;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void editar() {

        if (this.administradorSelecionado.getId() == 0) {
            this.dao.save(administradorSelecionado);
            this.addMessageInfo("Usuário salvo com sucesso!");
        } else {
            this.dao.update(administradorSelecionado);
            this.addMessageInfo("Usuário alterado com sucesso!");
        }

        this.pesquisa();
    }

    public void pesquisa() {

        try {
            this.lista = this.dao.findByFiltro(codigo, nome);

        } catch (Exception ex) {
            ex.printStackTrace();

        }

    }

    public void deletar(Administrador administrador) {
        try {
            this.dao.delete(administrador);
            this.addMessageInfo("Deletado com sucesso");
            this.pesquisa();
        } catch (Exception ex) {
            ex.printStackTrace();
            this.addMessageErro("Erro ao deletar!");
        }
    }

}
