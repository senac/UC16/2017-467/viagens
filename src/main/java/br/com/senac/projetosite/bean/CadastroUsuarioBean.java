
package br.com.senac.projetosite.bean;


import br.com.senac.projetosite.dao.UsuarioDAO;
import br.com.senac.projetosite.model.Usuario;
import java.io.Serializable;
import javax.faces.bean.ManagedProperty;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

@Named(value = "cadastrarUsuario")
@ViewScoped
public class CadastroUsuarioBean extends Bean implements Serializable{
    
    
    @ManagedProperty(value="#{loginBean.usuario}")
    private Usuario usuario;
    private UsuarioDAO dao;
    
    public CadastroUsuarioBean() {
        
        this.usuario = new Usuario();
        this.dao = new UsuarioDAO();
    }
    
   
  

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }
    
    
    
    
}
