package br.com.senac.projetosite.bean;


import br.com.senac.projetosite.dao.UsuarioDAO;
import br.com.senac.projetosite.model.Usuario;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.faces.view.ViewScoped;

@Named(value = "editarUsuarioBean")
@ViewScoped
public class EditarUsuarioBean extends Bean {

    private Usuario usuarioSelecionado;
    private UsuarioDAO dao;
    private List<Usuario> lista;
    
    private String codigo;
    private String nome;

    public EditarUsuarioBean() {
        
    }
    
    @PostConstruct
    public void init(){
        try{
            dao = new UsuarioDAO();
            usuarioSelecionado = new Usuario();
            lista = dao.findAll();
        }catch(Exception ex){
            ex.printStackTrace();
            this.addMessageErro("Falha ao carregar itens.");
        }
    }
    
    public void pesquisa(){
        try {
            this.lista = this.dao.findByFiltro(codigo, nome);

        }catch(Exception ex){
            ex.printStackTrace();
            
        }
    }
    
    public  void salvar(){
        if(this.usuarioSelecionado.getId() == 0){
            this.dao.save(usuarioSelecionado);
            this.addMessageInfo("Salvo com sucesso");
        }else{
            this.dao.update(usuarioSelecionado);
            this.addMessageInfo("Atualizado com sucesso");
        }
    }
    
    public void deletar(Usuario usuario){
        try{
            this.dao.delete(usuario);
            this.addMessageInfo("Deletado com sucesso");
            this.pesquisa();
        }catch(Exception ex){
            ex.printStackTrace();
            this.addMessageErro("Erro ao deletar!");
        }
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    
    
    public Usuario getUsuarioSelecionado() {
        return usuarioSelecionado;
    }

    public void setUsuarioSelecionado(Usuario usuarioSelecionado) {
        this.usuarioSelecionado = usuarioSelecionado;
    }

    public UsuarioDAO getDao() {
        return dao;
    }

    public void setDao(UsuarioDAO dao) {
        this.dao = dao;
    }

    public List<Usuario> getLista() {
        return lista;
    }

    public void setLista(List<Usuario> lista) {
        this.lista = lista;
    }
    
    

}
