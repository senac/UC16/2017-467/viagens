package br.com.senac.projetosite.bean;


import br.com.senac.projetosite.dao.UsuarioDAO;
import br.com.senac.projetosite.model.Usuario;
import javax.annotation.PostConstruct;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;

@Named(value = "loginBean")
@SessionScoped
public class LoginBean extends Bean{

    private Usuario usuario ;
    private Usuario usuarioLogado ; 
    private UsuarioDAO dao ;
    private boolean  logado ; 

    public LoginBean() {
    }
    
    @PostConstruct
    private void init(){
        usuarioLogado = new Usuario();
        usuario = new Usuario();
        dao = new UsuarioDAO() ; 
        
    }
    

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }
    
    public String logar(){
        usuarioLogado = dao.findByUsername(usuario.getEmail()) ; 
        
        if(this.usuario.getSenha().equals(usuarioLogado.getSenha()) ){
            logado = true  ;
            System.out.println("Logou ................");
        }
        
        return  "customer-register" ; 
    }
    private void novo(){
        this.usuarioLogado = new Usuario() ; 
    }
    
    public String logout(){
        
        this.novo() ; 
        logado = false  ;
        
        return  null   ; 
    }

    public String getNomeUsuarioLogado() {
        return usuarioLogado.getNome();
    }

    public Usuario getUsuarioLogado() {
        return usuarioLogado;
    }

    public void setUsuarioLogado(Usuario usuarioLogado) {
        this.usuarioLogado = usuarioLogado;
    }

    public boolean isLogado() {
        return logado;
    }

    
    
    
     public void salvar(){
        
        if (this.usuarioLogado.getId() == 0){
            dao.save(usuarioLogado);
            addMessageInfo("Salvo com sucesso");
            
        }else{
            dao.update(usuarioLogado);
        }
    }
     
    
    
    
    
    

}
