package br.com.senac.projetosite.model;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;

@MappedSuperclass
public abstract class Pessoa extends Entidade implements Serializable {

    @Column(nullable = false, length = 200)
    private String nome;
    
    @Column(nullable = false, length = 200)
    private String sobrenome;


    @Column(nullable = true, length = 200)
    private String email;

    @Column(nullable = false, unique = true, length = 11, insertable = true, updatable = false)
    private String cpf;
    
    @Column(nullable = true, length = 20)
    private Integer telefone;


    @Column(length = 500, nullable = false)
    private String endereco;

    @Column(nullable = true)
    private Integer numero;

    @Column(nullable = true, length = 500)
    private String complemento;

    @Column(nullable = true, length = 200)
    private String cidade;
    
    @Column(nullable = true, length = 20)
    private String cep;

    @Column(nullable = true, length = 2)
    private String estado;

    @Column(nullable = true, length = 200)
    private String senha;

    
    
    @Transient
    public String getEnderecoCompleto(){
        
        return this.endereco + " - " + "Nº " + numero + " - " + complemento + " - " + cidade + " - " + estado ;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
    
        public String getSobrenome() {
        return sobrenome;
    }

    public void setSobrenome(String sobrenome) {
        this.sobrenome = sobrenome;
    }


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public Integer getNumero() {
        return numero;
    }

    public void setNumero(Integer numero) {
        this.numero = numero;
    }

    public String getComplemento() {
        return complemento;
    }

    public void setComplemento(String complemento) {
        this.complemento = complemento;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }
    
    public Integer getTelefone() {
        return telefone;
    }

    public void setTelefone(Integer telefone) {
        this.telefone = telefone;
    }

    public String getCep() {
        return cep;
    }

    public void setCep(String cep) {
        this.cep = cep;
    }

}
