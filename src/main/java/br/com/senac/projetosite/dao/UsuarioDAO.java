
package br.com.senac.projetosite.dao;


import br.com.senac.projetosite.model.Usuario;
import java.util.List;
import javax.persistence.NoResultException;
import javax.persistence.Query;

public class UsuarioDAO extends DAO<Usuario>{
    
    public UsuarioDAO() {
        super(Usuario.class);
    }
    
    public static void main(String[] args) {
       /*Usuario usuario = new Usuario();
        usuario.setNome("Malandro");
        usuario.setCidade("Cariacica");
        usuario.setEmail("matheus@hotmail.com");
        usuario.setCpf("900000000");
        usuario.setEndereco("Rua Jose ");
        usuario.setNumero(200);
        usuario.setSenha("abc123");
        usuario.setComplemento("Quadra J");
        usuario.setEstado("ES");

        UsuarioDAO dao = new UsuarioDAO(); 
        
        dao.save(usuario);*/
      
    }
    
    
    public Usuario findByUsername (String userEmail){
        
         Usuario usuario = new Usuario() ; 
         try{
         this.em = JPAUtil.getEntityManager();
         
        
         Query query = this.em.createQuery("from Usuario u where u.email = :userEmail ") ; 
         query.setParameter("userEmail", userEmail) ; 
         usuario = (Usuario)query.getSingleResult() ; 
         
         
         this.em.close();
         }catch(NoResultException ex){
             ex.printStackTrace();
             return usuario ; 
         }
         
        return usuario ; 
    }
    
   public List<Usuario> findByFiltro(String codigo, String nome) {
        this.em = JPAUtil.getEntityManager();
        List<Usuario> lista;
        em.getTransaction().begin();

        StringBuilder sql = new StringBuilder("from Usuario a where 1=1 ");

        if (codigo != null && !codigo.isEmpty()) {
            sql.append(" and a.id = :Id ");
        }

        if (nome != null && !nome.isEmpty()) {
            sql.append(" and a.nome like :Nome ");
        }
        
         Query query = em.createQuery(sql.toString());
        
        if (codigo != null && !codigo.isEmpty()) {
           query.setParameter("Id", new Long(codigo));
        }

        if (nome != null && !nome.isEmpty()) {
            query.setParameter("Nome", "%" + nome + "%");
        }


        lista = query.getResultList();

        em.getTransaction().commit();
        em.close();

        return lista;
    }
}
