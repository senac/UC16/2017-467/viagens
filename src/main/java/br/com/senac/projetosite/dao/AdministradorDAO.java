package br.com.senac.projetosite.dao;

import br.com.senac.projetosite.model.Administrador;
import java.util.List;
import javax.persistence.NoResultException;
import javax.persistence.Query;

public class AdministradorDAO extends DAO<Administrador> {

    public AdministradorDAO() {
        super(Administrador.class);
    }

    public Administrador findByUsername(String admLoginAdm) {

        Administrador administrador = new Administrador();
        try {
            this.em = JPAUtil.getEntityManager();

            Query query = this.em.createQuery("from Administrador a where a.loginAdm = :admLoginAdm ");
            query.setParameter("admLoginAdm", admLoginAdm);
            administrador = (Administrador) query.getSingleResult();

            this.em.close();
        } catch (NoResultException ex) {
            ex.printStackTrace();
            return administrador;
        }

        return administrador;
    }

    public List<Administrador> findByFiltro(String codigo, String nome) {
        this.em = JPAUtil.getEntityManager();
        List<Administrador> lista;
        em.getTransaction().begin();

        StringBuilder sql = new StringBuilder("from Administrador a where 1=1 ");

        if (codigo != null && !codigo.isEmpty()) {
            sql.append(" and a.id = :Id ");
        }

        if (nome != null && !nome.isEmpty()) {
            sql.append(" and a.nome like :Nome ");
        }

        Query query = em.createQuery(sql.toString());

        if (codigo != null && !codigo.isEmpty()) {
            query.setParameter("Id", new Long(codigo));
        }

        if (nome != null && !nome.isEmpty()) {
            query.setParameter("Nome", "%" + nome + "%");
        }

        lista = query.getResultList();

        em.getTransaction().commit();
        em.close();

        return lista;
    }

}
